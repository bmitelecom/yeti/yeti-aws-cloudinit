#!/bin/bash

if ! [[ $LOCAL_IP ]]
then
  LOCAL_IP=`curl http://169.254.169.254/2009-04-04/meta-data/local-ipv4`
fi

if ! [[ $PUBLIC_IP ]]
then
  PUBLIC_IP=`curl http://169.254.169.254/2009-04-04/meta-data/public-ipv4`
fi

# Configure the main SEMS config
cat <<EOF > /etc/sems/sems.conf
node_id = 4

interfaces=intern
sip_ip_intern=$LOCAL_IP
sip_port_intern=5061
media_ip_intern=$LOCAL_IP
public_ip_intern=$PUBLIC_IP
rtp_low_port_intern=20000
rtp_high_port_intern=50000
plugin_path=/usr/lib/sems/plug-in/
load_plugins=wav;ilbc;speex;gsm;adpcm;l16;g722;sctp_bus;yeti;session_timer;uac_auth;di_log;registrar_client;jsonrpc
application = yeti
plugin_config_path=/etc/sems/etc/
fork=yes
stderr=no
syslog_loglevel=2
max_shutdown_time = 10

session_processor_threads=20
media_processor_threads=2
session_limit="4000;509;Node overloaded"
shutdown_mode_reply="508 Node in shutdown mode"
options_session_limit="900;503;Warning, server soon overloaded"
# cps_limit="100;503;Server overload"
use_raw_sockets=yes
sip_timer_B = 8000
default_bl_ttl=0
registrations_enabled=no
EOF

# Configure the yeti config
cat <<EOF > /etc/sems/etc/yeti.conf
# cfg_timeout
#       maximum time for waiting remote config (ms)
#       default: 5000
cfg_timeout=5000

# cfg_host
#       SCTP endpoint host
#       default: 127.0.0.1
cfg_host=127.0.0.1

# cfg_port
#       SCTP endpoint host
#       default: 4444
cfg_port=4444

# whether to sent 200OK for OPTIONS requests
core_options_handling=yes
EOF

cat <<EOF > /etc/sems/etc/jsonrpc.conf
# jsonrpc_listen  - json-rpc interface address to listen on
#
# optional; default: 127.0.0.1
#
jsonrpc_listen=127.0.0.1


# jsonrpc_port  - json-rpc server port to listen on
#
# optional; default: 7080
#
jsonrpc_port=7080


# server_threads  - json-rpc server threads to start
#
# optional; default: 5
#
# server_threads=5
EOF

# We have to patch service file in order to provide a delay
cat <<EOF > /lib/systemd/system/sems.service
[Unit]
Description=SEMS for YETI project
Documentation=https://yeti-switch.org/docs/

[Install]
WantedBy=multi-user.target

[Service]
ExecStartPre=/bin/sleep 15
User=root
LimitNOFILE=65536
LimitCORE=infinity

ExecStart=/usr/sbin/sems -P /var/run/sems.pid -u root -g root -f /etc/sems/sems.conf
PIDFile=/var/run/sems.pid
Type=forking
Restart=on-abnormal
EOF
