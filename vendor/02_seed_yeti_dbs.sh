#!/bin/bash

SD="/root/.pgpass"

YETI_USER=`head -n 1 $SD | cut -d":" -f4`
YETI_PASSWORD=`head -n 1 $SD | cut -d":" -f5`

CDR_USER=`tail -n 1 $SD | cut -d":" -f4`
CDR_PASSWORD=`tail -n 1 $SD | cut -d":" -f5`

if ! [[ $LOCAL_IP ]]
then
  export LOCAL_IP=`curl http://169.254.169.254/2009-04-04/meta-data/local-ipv4`
fi

if ! [[ $PUBLIC_IP ]]
then
  export PUBLIC_IP=`curl http://169.254.169.254/2009-04-04/meta-data/public-ipv4`
fi

# Configure the main SEMS config
echo "Create Yeti web database config"

cat <<EOF > /home/yeti-web/config/database.yml
production:
  adapter: postgresql
  encoding: unicode
  database: yeti
  pool: 5
  username: $YETI_USER
  password: $YETI_PASSWORD
  host: 127.0.0.1
  schema_search_path: >
    gui, public, switch,
    billing, class4, runtime_stats,
    sys, logs, data_import
  port: 5432
  #min_messages: warning

secondbase:
  production:
    adapter: postgresql
    encoding: unicode
    database: cdr
    pool: 5
    username: $CDR_USER
    password: $CDR_PASSWORD
    host: 127.0.0.1
    schema_search_path: 'cdr, reports, billing, public'
    port: 5432
    #min_messages: warning
EOF

# Create PGQd ticker configuration
echo "Create PGQD config"

cat <<EOF > /etc/pgqd.ini
[pgqd]
base_connstr = host=127.0.0.1 port=5432 dbname=cdr user=$CDR_USER password=$CDR_PASSWORD
initial_database = cdr
database_list = cdr
script = /usr/bin/pgqd
pidfile = /var/run/postgresql/pgqd.pid
ticker_max_count=1
ticker_max_lag=3
ticker_idle_period=360
EOF

# Init databases
cd /home/yeti-web

RAILS_ENV=production ./bin/bundle.sh exec rake db:structure:load db:migrate
RAILS_ENV=production ./bin/bundle.sh exec rake db:second_base:structure:load db:second_base:migrate
RAILS_ENV=production ./bin/bundle.sh exec rake db:seed

# Upgrade to the latest version
RAILS_ENV=production ./bin/bundle.sh exec rake db:migrate
RAILS_ENV=production ./bin/bundle.sh exec rake db:second_base:migrate

# We have to create a new POD and a Node
echo -ne "INSERT INTO sys.pops VALUES(2, 'Main');" | psql -h 127.0.0.1 -U $YETI_USER yeti
echo -ne "INSERT INTO sys.nodes VALUES(4, '$PUBLIC_IP', '5060', 'Node1', 2, '127.0.0.1:7080');" | psql -h 127.0.0.1 -U $YETI_USER yeti

