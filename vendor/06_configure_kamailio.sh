#!/bin/bash

if ! [[ $LOCAL_IP ]]
then
  LOCAL_IP=`curl http://169.254.169.254/2009-04-04/meta-data/local-ipv4`
fi

if ! [[ $PUBLIC_IP ]]
then
  PUBLIC_IP=`curl http://169.254.169.254/2009-04-04/meta-data/public-ipv4`
fi

PUBLIC_HOSTNAME=`curl http://169.254.169.254/2009-04-04/meta-data/public-hostname`

# Configure the dispatcher list
cat <<EOF > /etc/kamailio/dispatcher.list
# $Id$
# dispatcher destination sets
#

# line format
# setit(int) destination(sip uri) flags(int,opt) priority(int,opt) attributes(str,opt)

#yeti nodes
1 sip:$LOCAL_IP:5061
EOF

# Configure the LB config
cat <<EOF > /etc/kamailio/lb.cfg
#!subst "/GRP_ID/1/"

#!subst "/VER/YETI balancing node/"

listen=udp:$LOCAL_IP:5060 advertise $PUBLIC_IP:5060
listen=tcp:$LOCAL_IP:5060 advertise $PUBLIC_IP:5060

alias=$LOCAL_IP
alias=$PUBLIC_IP
alias=$PUBLIC_HOSTNAME

disable_tcp=no
auto_aliases=no
enable_tls=no
tcp_connection_lifetime=3605

children=4

#!define FR_INV_TIMER   120000
#!define FR_TIMER 100
#!define FR_INV_TIMER_REMOTE 120000
#!define FR_TIMER_REMOTE 300
#!define FR_TIMER_REMOTE_HIGH 2000

# BALANCE_ALG code meanings from dispatcher manual
# 0 - hash over callid
# 1 - hash over from URI.
# 2 - hash over to URI.
# 3 - hash over request-URI.
# 4 - round-robin (next destination).
# 5 - hash over authorization-username (Proxy-Authorization or "normal" authorization). If no username is found, round robin is used.
# 6 - random (using rand()).
# 7 - hash over the content of PVs string. Note: This works only when the parameter hash_pvar is set.
# 8 - use first destination (good for failover).
# 9 - use weight based load distribution. You have to set the attribute 'weight' per each address in destination set.
# 10 - use call load distribution. You have to set the attribute 'duid' (as an unique string id) per each address in destination set. Also, you must set parameters 'dstid_avp' and 'ds_hash_size'.

#!subst "/BALANCE_ALG/0/"
EOF

systemctl restart kamailio
