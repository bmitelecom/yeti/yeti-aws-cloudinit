#!/bin/bash

# This script creates datbases and users for the Yeti installation on this server
# then this data will be copied to the file /root/.db-yeti.data with secure permissions
# so that users could find authorization data for later usage.

YETI_USER="yeti"
YETI_PASSWORD=`pwgen 12 1`

CDR_USER="cdr"
CDR_PASSWORD=`pwgen 12 1`

GUI_USER="cdr"
GUI_PASSWORD=`pwgen 12 1`

echo -ne "127.0.0.1:5432:yeti:$YETI_USER:$YETI_PASSWORD\n127.0.0.1:5432:cdr:$CDR_USER:$CDR_PASSWORD" > /root/.pgpass
chmod 600 /root/.pgpass

# Create DBs and users here
echo -ne "create user $YETI_USER encrypted password '$YETI_PASSWORD' superuser;" | su postgres -c psql
echo -ne "create database yeti owner $YETI_USER;" | su postgres -c psql

echo -ne "create user $CDR_USER encrypted password '$CDR_PASSWORD' superuser;" | su postgres -c psql
echo -ne "create database cdr owner $CDR_USER;" | su postgres -c psql

echo "################################ YETI WEB PANEL PASSWORD ###########################"
echo "#                                                                                  "
echo "#                                     USER: admin                                  "
echo "#                                     PASSWORD: $GUI_PASSWORD                      "
echo "#                                                                                  "
echo "####################################################################################"

# Export the INSTALL_ID variable so we enable user to report us current installation ID
export INSTALL_ID=`uuidgen -r`
echo -ne $INSTALL_ID > /root/.install_id
chmod 600 /root/.install_id

#Here we fix the original seeds file in order to convey the new hash
GUI_PWD_HASH=`htpasswd -bnBC 10 "" $GUI_PASSWORD | tr -d ':\n' | sed 's/$2y/$2a/'`
sed -i s%'$2a$10$2346aIc.UfYbcoRUET4Fwuaqg573IrYcK2dnmxtdg2JC6OqJxK4U2'%"$GUI_PWD_HASH"% /home/yeti-web/db/seeds/main/gui.sql

