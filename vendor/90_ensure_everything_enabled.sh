#!/bin/bash
SERVICES=(postgresql redis-server yeti-web yeti-cdr-billing@cdr_billing yeti-delayed-job pgqd yeti-management sems kamailio)

for ser in ${SERVICES[@]}
do
  systemctl -q enable $ser
  if [[ $? -ne 0 ]]
  then
    echo "Service $ser is not enabled"
    exit 1
  else
    echo "Server $ser has been successfuly enabled"
    continue
  fi
done
