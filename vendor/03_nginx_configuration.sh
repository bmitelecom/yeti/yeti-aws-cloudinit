#!/bin/bash

set -eo

rm -f /etc/nginx/sites-enabled/default
cp /home/yeti-web/config/yeti-web.dist.nginx /etc/nginx/sites-enabled/yeti

nginx -t
if [[ $? -eq 0 ]]
then
    echo "Succesfully copied the nginx config"
    nginx -s reload
else
    echo "Something went wrong"
fi

