#!/bin/bash

SERVICES=(postgresql redis-server yeti-web yeti-cdr-billing@cdr_billing yeti-delayed-job pgqd yeti-management sems kamailio)

for ser in ${SERVICES[@]}
do
  systemctl -q status $ser
  if [[ $? -ne 0 ]]
  then
    echo "Service $ser is not started."
    systemctl start $ser
    if [[ $? -ne 0 ]]
    then
      echo "Something went wrong during $ser start. Please, check $ser logs or make a ticket to BMI Telecom support"
      systemctl start $ser
      if [[ $? -ne 0 ]]
      then
        continue
      fi
    else
      echo "Server $ser has been successfuly started"
      continue
    fi
  fi
done
